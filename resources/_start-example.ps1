Clear-Host

$executable = "Server.exe"
$installDir = Split-Path $PSCommandPath -Parent
$logFile = "$installDir\Logs\output_log.txt"
$args = ''

do {
    $process = Get-Process | Where-Object { $_.Path -Like "*$executable*" } -ErrorAction SilentlyContinue
    if ($process) {
        Write-Host "Stopping existing server(s)...`n"
        $process | Stop-Process -Force; Start-Sleep 5
    }

    Write-Host "Starting server...`n"
    $serverJob = Start-Job -ScriptBlock { Start-Process "$using:installDir\$using:executable" -ArgumentList $using:args -Wait }

    $logJob = Start-Job -ScriptBlock { Get-Content "$using:logFile" -Wait }
    while ($serverJob.State -eq 'Running' -And $logJob.HasMoreData) {
      Receive-Job $logJob
      Start-Sleep -Milliseconds 200
    }

    Receive-Job $logJob; Stop-Job $logJob; Remove-Job $logJob; Remove-Job $serverJob -Force
    Write-Host "Restarting server...`n"; Start-Sleep 5
} while (!($serverJob.State -eq 'Running'))
