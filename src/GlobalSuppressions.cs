﻿// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.IOnUserApprove(CodeHatch.Engine.Networking.Player)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.IOnPlayerChat(CodeHatch.Networking.Events.Players.PlayerMessageEvent)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.IOnPlayerCommand(CodeHatch.Networking.Events.Players.PlayerCommandEvent)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.IOnPlayerConnected(CodeHatch.Engine.Networking.Player)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.IOnPlayerDisconnected(CodeHatch.Engine.Networking.Player)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.IOnServerCommand(CodeHatch.Engine.Core.Commands.CommandInfo)~System.Object")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnPlayerRespawn(CodeHatch.Networking.Events.Players.PlayerRespawnEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnPlayerSpawn(CodeHatch.Networking.Events.Players.PlayerFirstSpawnEvent)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnPlayerSpawned(CodeHatch.Networking.Events.Players.PlayerPreSpawnCompleteEvent)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Ban(System.String,System.TimeSpan)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Kick(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Heal(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Hurt(System.Single)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Rename(System.String)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Respawn(uMod.Common.Position)")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Teleport(System.Single,System.Single,System.Single)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.Init")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnPluginLoaded(uMod.Plugins.Plugin)")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnServerInitialized")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnServerSave")]
[assembly: SuppressMessage("CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKings.OnServerShutdown")]
[assembly: SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification = "<Pending>", Scope = "member", Target = "~M:uMod.ReignOfKings.ReignOfKingsConsolePlayer.Teleport(uMod.Common.Position)")]
