﻿using CodeHatch.Common;
using CodeHatch.Damaging;
using CodeHatch.Engine.Behaviours;
using CodeHatch.Engine.Core.Commands;
using CodeHatch.Engine.Networking;
using CodeHatch.Networking.Events;
using CodeHatch.Networking.Events.Entities;
using CodeHatch.StarForge.Sleeping;
using System;
using System.Globalization;
using uMod.Auth;
using uMod.Common;
using uMod.Text;
using UnityEngine;
using Player = CodeHatch.Engine.Networking.Player;

namespace uMod.Game.ReignOfKings
{
    /// <summary>
    /// Represents a player, either connected or not
    /// </summary>
    public class ReignOfKingsPlayer : UniversalPlayer, IPlayer
    {
        #region Initialization

        private readonly ulong steamId;

        internal Player player;

        public ReignOfKingsPlayer(string playerId, string playerName)
        {
            // Store player details
            Id = playerId;
            Name = playerName.Sanitize();
            ulong.TryParse(playerId, out steamId);
        }

        public ReignOfKingsPlayer(Player player) : this(player.Id.ToString(), player.Name)
        {
            // Store player object
            this.player = player;
        }

        #endregion Initialization

        #region Objects

        /// <summary>
        /// Gets the object that backs the player
        /// </summary>
        public object Object { get; set; }

        /// <summary>
        /// Gets the player's last command type
        /// </summary>
        public CommandType LastCommand { get; set; }

        /// <summary>
        /// Reconnects the gamePlayer to the player object
        /// </summary>
        /// <param name="gamePlayer"></param>
        public void Reconnect(object gamePlayer)
        {
            // Reconnect player objects
            Object = gamePlayer;
            player = gamePlayer as Player;
        }

        #endregion Objects

        #region Information

        /// <summary>
        /// Gets/sets the name for the player
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets the ID for the player (unique within the current game)
        /// </summary>
        public override string Id { get; }

        /// <summary>
        /// Gets the language for the player
        /// </summary>
        public CultureInfo Language => throw new NotImplementedException(); // TODO: Implement when possible

        /// <summary>
        /// Gets the IP address for the player
        /// </summary>
        public string Address => player?.Connection?.IpAddress ?? string.Empty;

        /// <summary>
        /// Gets the average network ping for the player
        /// </summary>
        public int Ping => player?.Connection?.AveragePing ?? 0;

        /// <summary>
        /// Gets if the player is a server admin
        /// </summary>
        public override bool IsAdmin => player?.HasPermission("admin") ?? false;

        /// <summary>
        /// Gets if the player is a server moderator
        /// </summary>
        public override bool IsModerator => IsAdmin;

        /// <summary>
        /// Gets if the player is banned
        /// </summary>
        public bool IsBanned => Server.IdIsBanned(steamId);

        /// <summary>
        /// Gets if the player is connected
        /// </summary>
        public bool IsConnected => player?.Connection?.IsConnected ?? false;

        /// <summary>
        /// Gets if the player is alive
        /// </summary>
        public bool IsAlive => player?.IsAlive() ?? false;

        /// <summary>
        /// Gets if the player is dead
        /// </summary>
        public bool IsDead => !IsAlive;

        /// <summary>
        /// Gets if the player is sleeping
        /// </summary>
        public bool IsSleeping
        {
            get
            {
                ISleeper sleeper = player?.Entity.Get<ISleeper>();
                return sleeper != null && sleeper.IsSleeping;
            }
        }

        /// <summary>
        /// Gets if the player is the server
        /// </summary>
        public bool IsServer => false;

        /// <summary>
        /// Gets/sets if the player has connected before
        /// </summary>
        public bool IsReturningPlayer { get; set; }

        /// <summary>
        /// Gets/sets if the player has spawned before
        /// </summary>
        public bool HasSpawnedBefore { get; set; }

        #endregion Information

        #region Administration

        /// <summary>
        /// Bans the player for the specified reason and duration
        /// </summary>
        /// <param name="reason"></param>
        /// <param name="duration"></param>
        public void Ban(string reason = "", TimeSpan duration = default)
        {
            // Check if already banned
            if (!IsBanned)
            {
                // Ban and kick player with reason
                Server.Ban(player, (int)duration.TotalSeconds, reason);
            }
        }

        /// <summary>
        /// Gets the amount of time remaining on the player's ban
        /// </summary>
        public TimeSpan BanTimeRemaining => new DateTime(Server.GetBannedPlayerFromPlayerId(steamId).ExpireDate) - DateTime.Now;

        /// <summary>
        /// Kicks the player from the server
        /// </summary>
        /// <param name="reason"></param>
        public void Kick(string reason = "")
        {
            if (IsConnected)
            {
                Server.Kick(player, reason);
            }
        }

        /// <summary>
        /// Unbans the player
        /// </summary>
        public void Unban()
        {
            // Check if already unbanned
            if (IsBanned)
            {
                // Unban player
                Server.Unban(steamId);
            }
        }

        #endregion Administration

        #region Character

        /// <summary>
        /// Gets/sets the player's health
        /// </summary>
        public float Health
        {
            get => player?.GetHealth()?.CurrentHealth ?? 0f;
            set
            {
                if (player != null)
                {
                    player.GetHealth().CurrentHealth = value;
                }
            }
        }

        /// <summary>
        /// Gets/sets the player's maximum health
        /// </summary>
        public float MaxHealth
        {
            get => player?.GetHealth()?.MaxHealth ?? 0f;
            set
            {
                if (player != null)
                {
                    player.GetHealth().MaxHealth = value;
                }
            }
        }

        /// <summary>
        /// Heals the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Heal(float amount) => player?.Heal(amount);

        /// <summary>
        /// Damages the player's character by specified amount
        /// </summary>
        /// <param name="amount"></param>
        public void Hurt(float amount)
        {
            if (player?.CurrentCharacter != null && player.Entity != null)
            {
                Damage damage = new Damage(player.CurrentCharacter.Prefab, null)
                {
                    Amount = amount,
                    DamageTypes = DamageType.Unknown,
                    Damager = player.Entity
                };
                EventManager.CallEvent(new EntityDamageEvent(player.Entity, damage));
            }
        }

        /// <summary>
        /// Causes the player's character to die
        /// </summary>
        public void Kill() => player?.Kill();

        /// <summary>
        /// Renames the player to specified name
        /// <param name="newName"></param>
        /// </summary>
        public void Rename(string newName) => player?.CurrentCharacter?.ChangeName(newName);

        /// <summary>
        /// Resets the player's character stats
        /// </summary>
        public void Reset()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character
        /// </summary>
        public void Respawn()
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        /// <summary>
        /// Respawns the player's character at specified position
        /// </summary>
        public void Respawn(Position pos)
        {
            throw new NotImplementedException(); // TODO: Implement when possible
        }

        #endregion Character

        #region Positional

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <returns></returns>
        public Position Position()
        {
            Vector3 pos = player?.Entity?.Position ?? new Vector3();
            return new Position(pos.x, pos.y, pos.z);
        }

        /// <summary>
        /// Gets the position of the player
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Position(out float x, out float y, out float z)
        {
            Position pos = Position();
            x = pos.X;
            y = pos.Y;
            z = pos.Z;
        }

        /// <summary>
        /// Teleports the player's character to the specified position
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public void Teleport(float x, float y, float z)
        {
            if (IsConnected && player?.Entity != null)
            {
                player.Entity.GetOrCreate<CharacterTeleport>()?.Teleport(new Vector3(x, y, z));
            }
        }

        /// <summary>
        /// Teleports the player's character to the specified generic position
        /// </summary>
        /// <param name="pos"></param>
        public void Teleport(Position pos) => Teleport(pos.X, pos.Y, pos.Z);

        #endregion Positional

        #region Chat and Commands

        /// <summary>
        /// Sends the specified message and prefix to the player
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Message(string message, string prefix, params object[] args)
        {
            if (!string.IsNullOrEmpty(message) && IsConnected)
            {
                message = args.Length > 0 ? string.Format(Formatter.ToRoKAnd7DTD(message), args) : Formatter.ToRoKAnd7DTD(message);
                player?.SendMessage(prefix != null ? $"{prefix} {message}" : message);
            }
        }

        /// <summary>
        /// Sends the specified message to the player
        /// </summary>
        /// <param name="message"></param>
        public void Message(string message) => Message(message, null);

        /// <summary>
        /// Replies to the player with the specified message and prefix
        /// </summary>
        /// <param name="message"></param>
        /// <param name="prefix"></param>
        /// <param name="args"></param>
        public void Reply(string message, string prefix, params object[] args) => Message(message, prefix, args);

        /// <summary>
        /// Replies to the player with the specified message
        /// </summary>
        /// <param name="message"></param>
        public void Reply(string message) => Message(message, null);

        /// <summary>
        /// Runs the specified console command on the player
        /// </summary>
        /// <param name="command"></param>
        /// <param name="args"></param>
        public void Command(string command, params object[] args)
        {
            if (!string.IsNullOrEmpty(command) && IsConnected)
            {
                CommandManager.ExecuteCommand(steamId, $"/{command} {string.Join(" ", Array.ConvertAll(args, x => x.ToString()))}");
            }
        }

        #endregion Chat and Commands
    }
}
