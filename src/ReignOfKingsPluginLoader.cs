﻿using System;
using uMod.Common;
using uMod.Plugins;

namespace uMod.Game.ReignOfKings
{
    /// <summary>
    /// Responsible for loading the core plugin
    /// </summary>
    public class ReignOfKingsPluginLoader : PluginLoader
    {
        public override Type[] CorePlugins => new[] { typeof(ReignOfKings) };

        public ReignOfKingsPluginLoader(ILogger logger) : base(logger)
        {
        }
    }
}
