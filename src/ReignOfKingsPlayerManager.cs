﻿using uMod.Auth;
using uMod.Common;

namespace uMod.Game.ReignOfKings
{
    /// <summary>
    /// Represents a ReignOfKings player manager
    /// </summary>
    public class ReignOfKingsPlayerManager : PlayerManager<ReignOfKingsPlayer>
    {
        /// <summary>
        /// Create a new instance of the ReignOfKingsPlayerManager class
        /// </summary>
        /// <param name="application"></param>
        /// <param name="logger"></param>
        public ReignOfKingsPlayerManager(IApplication application, ILogger logger) : base(application, logger)
        {
        }
    }
}
