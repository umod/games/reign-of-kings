using CodeHatch.Engine.Core.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using uMod.Command;
using uMod.Common;
using uMod.Common.Command;
using CommandAttribute = CodeHatch.Engine.Core.Commands.CommandAttribute;

namespace uMod.Game.ReignOfKings
{
    /// <summary>
    /// Represents a binding to a generic command system
    /// </summary>
    public class ReignOfKingsCommands : ICommandSystem
    {
        #region Initialization

        // The universal provider
        private readonly ReignOfKingsProvider Universal = ReignOfKingsProvider.Instance;

        // The console player
        internal readonly ReignOfKingsConsolePlayer ConsolePlayer;

        // Command handler
        internal readonly ICommandHandler CommandHandler;

        // Default universal commands
        internal readonly Commands DefaultCommands;

        // All registered commands
        internal IDictionary<string, RegisteredCommand> RegisteredCommands;

        // Registered commands
        internal class RegisteredCommand
        {
            /// <summary>
            /// The plugin that handles the command
            /// </summary>
            public readonly IPlugin Source;

            /// <summary>
            /// The name of the command
            /// </summary>
            public readonly string Command;

            /// <summary>
            /// The callback
            /// </summary>
            public readonly CommandCallback Callback;

            /// <summary>
            /// The original callback
            /// </summary>
            public CommandAttribute OriginalCallback;

            /// <summary>
            /// Initializes a new instance of the RegisteredCommand class
            /// </summary>
            /// <param name="source"></param>
            /// <param name="command"></param>
            /// <param name="callback"></param>
            public RegisteredCommand(IPlugin source, string command, CommandCallback callback)
            {
                Source = source;
                Command = command;
                Callback = callback;
            }
        }

        /// <summary>
        /// Initializes the command system
        /// </summary>
        /// <param name="commandHandler"></param>
        public ReignOfKingsCommands(ICommandHandler commandHandler)
        {
            RegisteredCommands = new Dictionary<string, RegisteredCommand>();
            CommandHandler = commandHandler;
            CommandHandler.Callback = CommandCallback;
            CommandHandler.CommandFilter = RegisteredCommands.ContainsKey;
            ConsolePlayer = new ReignOfKingsConsolePlayer();
            DefaultCommands = new Commands();
        }

        public CommandState CommandCallback(IPlayer caller, string command, string fullCommand, object[] context = null, ICommandInfo commandInfo = null)
        {
            if (!RegisteredCommands.TryGetValue(command, out RegisteredCommand registeredCommand))
            {
                return CommandState.Unrecognized;
            }

            return registeredCommand.Callback(caller, command, fullCommand, context, commandInfo);
        }

        #endregion Initialization

        #region Command Registration

        /// <summary>
        /// Registers the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        /// <param name="callback"></param>
        public void RegisterCommand(string command, IPlugin plugin, CommandCallback callback)
        {
            // Convert the command to lowercase and remove whitespace
            command = command.ToLowerInvariant().Trim();

            // Check if the command can be overridden
            if (!CanOverrideCommand(command))
            {
                throw new CommandAlreadyExistsException(command);
            }

            // Set up a new command
            RegisteredCommand newCommand = new RegisteredCommand(plugin, command, callback);

            // Check if the command already exists in another universal plugin
            if (RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd))
            {
                if (cmd.OriginalCallback != null)
                {
                    newCommand.OriginalCallback = cmd.OriginalCallback;
                }

                string newPluginName = plugin?.Name ?? "An unknown plugin"; // TODO: Localization
                string previousPluginName = cmd.Source?.Name ?? "an unknown plugin"; // TODO: Localization
                Interface.uMod.LogWarning($"{newPluginName} has replaced the '{command}' command previously registered by {previousPluginName}"); // TODO: Localization
            }

            // Check if the command already exists as a native command
            if (CommandManager.RegisteredCommands.ContainsKey(command))
            {
                if (newCommand.OriginalCallback == null)
                {
                    newCommand.OriginalCallback = CommandManager.RegisteredCommands[command];
                }

                CommandManager.RegisteredCommands.Remove(command);
                if (cmd == null)
                {
                    string newPluginName = plugin?.Name ?? "An unknown plugin"; // TODO: Localization
                    Interface.uMod.LogWarning($"{newPluginName} has replaced the '{command}' command previously registered by {Universal.GameName}"); // TODO: Localization
                }
            }

            // Register the command
            RegisteredCommands[command] = newCommand;
            CommandManager.RegisteredCommands[command] = new CommandAttribute("/" + command, string.Empty)
            {
                Method = (Action<CommandInfo>)Delegate.CreateDelegate(typeof(Action<CommandInfo>), this, GetType().GetMethod(nameof(HandleCommand), BindingFlags.NonPublic | BindingFlags.Instance))
            };
        }

        private void HandleCommand(CommandInfo cmdInfo)
        {
            if (RegisteredCommands.TryGetValue(cmdInfo.Label.ToLowerInvariant(), out RegisteredCommand _))
            {
                IPlayer player = cmdInfo.Player.IPlayer ?? ConsolePlayer as IPlayer;
                HandleChatMessage(player, $"/{cmdInfo.Label} {string.Join(" ", cmdInfo.Args)}");
            }
        }

        #endregion Command Registration

        #region Command Unregistration

        /// <summary>
        /// Unregisters the specified command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="plugin"></param>
        public void UnregisterCommand(string command, IPlugin plugin)
        {
            if (RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd))
            {
                // Check if the command belongs to the plugin
                if (plugin == cmd.Source)
                {
                    // Remove the command
                    RegisteredCommands.Remove(command);

                    // If this was originally a native Reign of Kings command then restore it, otherwise remove it
                    if (cmd.OriginalCallback != null)
                    {
                        CommandManager.RegisteredCommands[cmd.Command] = cmd.OriginalCallback;
                    }
                    else
                    {
                        CommandManager.RegisteredCommands.Remove(cmd.Command);
                    }
                }
            }
        }

        #endregion Command Unregistration

        #region Command Handling

        /// <summary>
        /// Checks if a command can be overridden
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        private bool CanOverrideCommand(string command)
        {
            if (!RegisteredCommands.TryGetValue(command, out RegisteredCommand cmd) || !cmd.Source.IsCorePlugin)
            {
                return !ReignOfKingsExtension.RestrictedCommands.Contains(command);
            }

            return true;
        }

        /// <summary>
        /// Handles a chat message
        /// </summary>
        /// <param name="player"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public CommandState HandleChatMessage(IPlayer player, string message) => CommandHandler.HandleChatMessage(player, message);

        #endregion Command Handling
    }
}
