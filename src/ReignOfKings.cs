using CodeHatch.Common;
using CodeHatch.Engine.Core.Commands;
using CodeHatch.Engine.Networking;
using CodeHatch.Networking.Events.Players;
using System;
using uMod.Common;
using uMod.Configuration;
using uMod.Plugins;
using uMod.Plugins.Decorators;
using CommandInfo = CodeHatch.Engine.Core.Commands.CommandInfo;

namespace uMod.Game.ReignOfKings
{
    /// <summary>
    /// The core Reign of Kings plugin
    /// </summary>
    [HookDecorator(typeof(ServerDecorator))]
    public class ReignOfKings : Plugin
    {
        #region Initialization

        internal static readonly ReignOfKingsProvider Universal = ReignOfKingsProvider.Instance;

        /// <summary>
        /// Initializes a new instance of the ReignOfKings class
        /// </summary>
        public ReignOfKings()
        {
            // Set plugin info attributes
            Title = "Reign of Kings";
            Author = ReignOfKingsExtension.AssemblyAuthors;
            Version = ReignOfKingsExtension.AssemblyVersion;

            CommandManager.OnRegisterCommand += attribute =>
            {
                foreach (string command in attribute.Aliases.InsertItem(attribute.Name, 0))
                {
                    if (Universal.CommandSystem.RegisteredCommands.TryGetValue(command, out ReignOfKingsCommands.RegisteredCommand universalCommand))
                    {
                        Universal.CommandSystem.RegisteredCommands.Remove(universalCommand.Command);
                        Universal.CommandSystem.RegisterCommand(universalCommand.Command, universalCommand.Source, universalCommand.Callback);
                    }
                }
            };
        }

        /// <summary>
        /// Called when the server is first initialized
        /// </summary>
        [Hook("OnServerInitialized")]
        private void OnServerInitialized()
        {
            // Add universal commands
            Universal.CommandSystem.DefaultCommands.Initialize(this);

            // Clean up invalid permission data
            permission.RegisterValidate(ExtensionMethods.IsSteamId);
            permission.CleanUp();
        }

        #endregion Initialization

        #region Player Hooks

        /// <summary>
        /// Called when the player is attempting to connect
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        [Hook("IOnUserApprove")]
        private object IOnUserApprove(Player player)
        {
            // Ignore the server player
            if (player.Id == 9999999999)
            {
                return null;
            }

            string playerId = player.Id.ToString();
            string ipAddress = player.Connection.IpAddress;

            Players.PlayerJoin(playerId, player.Name);

            // Call pre hook for plugins
            object loginUniversal = Interface.CallHook("CanClientLogin", player.Name, playerId, ipAddress);
            object loginDeprecated = Interface.CallDeprecatedHook("CanUserLogin", "CanClientLogin", new DateTime(2021, 1, 1), player.Name, playerId, ipAddress);
            object canLogin = loginUniversal is null ? loginDeprecated : loginUniversal;

            // Can the player log in?
            if (canLogin is string || canLogin is bool loginBlocked && !loginBlocked)
            {
                // Reject the player with message
                player.ShowPopup("Disconnected", canLogin is string ? canLogin.ToString() : "Connection was rejected"); // TODO: Localization
                player.Connection.Close();
                return ConnectionError.NoError;
            }

            // Call post hook for plugins
            Interface.CallHook("OnPlayerApproved", player.Name, playerId, ipAddress);
            Interface.CallDeprecatedHook("OnUserApprove", "OnPlayerApproved", new DateTime(2021, 1, 1), player);
            Interface.CallDeprecatedHook("OnUserApproved", "OnPlayerApproved", new DateTime(2021, 1, 1), player.Name, playerId, ipAddress);

            return null;
        }

        /// <summary>
        /// Called when the player sends a message
        /// </summary>
        /// <param name="evt"></param>
        /// <returns></returns>
        [Hook("IOnPlayerChat")]
        private object IOnPlayerChat(PlayerMessageEvent evt)
        {
            // Ignore the server player
            if (evt.PlayerId == 9999999999)
            {
                return null;
            }

            // Call hook for plugins
            object chatUniversal = Interface.CallHook("OnPlayerChat", evt.Player.IPlayer, evt.Message);
            object chatDeprecated = Interface.CallDeprecatedHook("OnUserChat", "OnPlayerChat", new DateTime(2021, 1, 1), evt.Player.IPlayer, evt.Message);
            object canChat = chatUniversal is null ? chatDeprecated : chatUniversal;

            // Is the chat message blocked?
            if (canChat != null)
            {
                // Cancel message event
                evt.Cancel();
                return true;
            }

            return null;
        }

        /// <summary>
        /// Called when the player runs a command
        /// </summary>
        /// <param name="evt"></param>
        [Hook("IOnPlayerCommand")]
        private object IOnPlayerCommand(PlayerCommandEvent evt)
        {
            // Ignore the server player
            if (evt.Sender.Equals(CodeHatch.Engine.Networking.Server.ServerPlayer))
            {
                return null;
            }

            // Is the player commanad blocked?
            if (Interface.CallHook("OnPlayerCommand", evt.Sender.IPlayer, evt.Label, evt.CommandArgs) != null)
            {
                // Cancel command event
                evt.Cancel();
                return true;
            }

            return null;
        }

        /// <summary>
        /// Called when the player has connected
        /// </summary>
        /// <param name="rokPlayer"></param>
        /// <returns></returns>
        [Hook("IOnPlayerConnected")]
        private void IOnPlayerConnected(Player rokPlayer)
        {
            // Ignore the server player
            if (rokPlayer.Id == 9999999999)
            {
                return;
            }

            string playerId = rokPlayer.Id.ToString();

            // Add player to default groups
            GroupProvider defaultGroups = Interface.uMod.Auth.Configuration.Groups;
            if (!permission.UserHasGroup(playerId, defaultGroups.Players))
            {
                permission.AddUserGroup(playerId, defaultGroups.Players);
            }
            if (rokPlayer.HasPermission("admin") && !permission.UserHasGroup(playerId, defaultGroups.Administrators))
            {
                permission.AddUserGroup(playerId, defaultGroups.Administrators);
            }

            IPlayer player = Players.FindPlayerById(playerId);
            if (player != null)
            {
                // Set IPlayer object in Player
                rokPlayer.IPlayer = player;

                Players.PlayerConnected(player.Id, player);

                // Call hook for plugins
                Interface.CallHook("OnPlayerConnected", player);
                Interface.CallDeprecatedHook("OnUserConnected", "OnPlayerConnected", new DateTime(2021, 1, 1), player);
            }
        }

        /// <summary>
        /// Called when the player has disconnected
        /// </summary>
        /// <param name="rokPlayer"></param>
        [Hook("IOnPlayerDisconnected")]
        private void IOnPlayerDisconnected(Player rokPlayer)
        {
            // Ignore the server player
            if (rokPlayer.Id == 9999999999)
            {
                return;
            }

            IPlayer player = rokPlayer.IPlayer;
            if (player != null)
            {
                // Call hook for plugins
                Interface.CallHook("OnPlayerDisconnected", player);
                Interface.CallDeprecatedHook("OnUserDisconnected", "OnPlayerDisconnected", new DateTime(2021, 1, 1), player.Name ?? "Unnamed", player.Id, player.Address ?? "0"); // TODO: Localization

                Players.PlayerDisconnected(player.Id);
            }
        }

        /// <summary>
        /// Called when the player is respawning
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerRespawn")] // Not being called every time?
        private void OnPlayerRespawn(PlayerRespawnEvent evt)
        {
            // Call hook for plugins
            Interface.CallHook("OnPlayerRespawn", evt.Player.IPlayer);
            Interface.CallDeprecatedHook("OnUserRespawn", "OnPlayerRespawn", new DateTime(2021, 1, 1), evt.Player.IPlayer);
        }

        /// <summary>
        /// Called when the player is spawning
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerSpawn")]
        private void OnPlayerSpawn(PlayerFirstSpawnEvent evt)
        {
            // Call hook for plugins
            Interface.CallHook("OnPlayerSpawn", evt.Player.IPlayer);
            Interface.CallDeprecatedHook("OnUserSpawn", "OnPlayerSpawn", new DateTime(2021, 1, 1), evt.Player.IPlayer);
        }

        /// <summary>
        /// Called when the player has spawned
        /// </summary>
        /// <param name="evt"></param>
        [Hook("OnPlayerSpawned")]
        private void OnPlayerSpawned(PlayerPreSpawnCompleteEvent evt)
        {
            // Call hook for plugins
            Interface.CallHook("OnPlayerSpawned", evt.Player.IPlayer);
            Interface.CallDeprecatedHook("OnUserSpawned", "OnPlayerSpawned", new DateTime(2021, 1, 1), evt.Player.IPlayer);
        }

        #endregion Player Hooks

        #region Server Hooks

        /// <summary>
        /// Called when a command is ran on the server
        /// </summary>
        /// <param name="cmdInfo"></param>
        /// <returns></returns>
        [Hook("IOnServerCommand")]
        private object IOnServerCommand(CommandInfo cmdInfo)
        {
            // Is the server command blocked?
            if (Interface.CallHook("OnServerCommand", cmdInfo.Label.ToLower(), string.Join(" ", Array.ConvertAll(cmdInfo.Args, x => x.ToString()))) != null)
            {
                return true;
            }

            return null;
        }

        #endregion Server Hooks
    }
}
